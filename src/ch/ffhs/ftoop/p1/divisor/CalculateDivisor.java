package ch.ffhs.ftoop.p1.divisor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Das folgende Programm soll aus einem vorgegebene Interval von Long-Zahlen die
 * Zahl zurückgeben, die die meisten Divisoren hat.
 * --> UMGESETZT sodass nur Primzahlen als Divisoren akzeptiert werden. U.a. kommt der Modulo zum Einsatz.
 *               Algortihmus beschr�nkt sich auf m�glichst wenige zu pr�fende Zahlen, f�r maximale Performacne.
 * 
 * Sollte es mehrere solche Zahlen geben, so soll die kleinste dieser Zahlen ausgegeben werden.
 * --> UMGESETZT mit compareTo() Methode aus Comparable Interface innerhalb der Klasse DivisorResult.
 * 
 * Die Berechnung soll in n Threads stattfinden, die via Executor Framework
 * gesteuert werden, und sich das Problem aufteilen - jeder Thread soll eine
 * Teilmenge des Problems lösen. Verwenden Sie bitte einen FixedThreadPool und
 * implementieren Sie die Worker als Callable.
 * --> UMGESETZT wie gefordert.
 * 
 * @author Daniel Hoop & Mehrdad Zendehzaban
 */
public class CalculateDivisor {

	long von, bis;
	int threadCount;
	TreeSet<DivisorResult> resultSet;
	boolean debugPrint = false;

	/**
	 * @param von
	 *            untere Intervallgrenze
	 * @param bis
	 *            obere Intervallgrenze
	 * @param threadCount
	 *            Abzahl der Threads, auf die das Problem aufgeteilt werden soll
	 */
	public CalculateDivisor(long von, long bis, int threadCount) {
		this.von = von;
		this.bis = bis;
		this.threadCount = threadCount;
		this.resultSet = new TreeSet<DivisorResult>();
	}
	
	
	/**
	 * Berechnungsroutine
	 * In der Methode calculateCallable ist die eigentliche kalkulation der Anzahl Divisoren umgesetzt.
	 * Als Callable kann es dann dem Executor-Framework �bergeben werden. 
	 * @author Daniel Hoop & Mehrdad Zendehzaban
	 *
	 */
	public class calculateCallable implements Callable<DivisorResult>{
		long zahl;
		HashSet<Long> allDivisors;
		
		public calculateCallable(long zahl){
			this.zahl = zahl;
			allDivisors = new HashSet<Long>();
		}
		
		public DivisorResult call() {
			long divisor, nDivisors = 0;
			boolean acceptNewDivisor = true;
			
			// Am Anfang pruefen, ob durch 2 teilbar. Danach kann man sich alle vielfachen von 2 sparen.
			if(zahl%2==0){
				if(debugPrint) System.out.println("New Primzahl-Divisor found: " + 2);
				nDivisors = 1;
				allDivisors.add((long) 2);
			}
			// Der Loop muss nur bis zur H�lfte der Zahl laufen. Danach gibt es per Definition immer etwas nicht ganzzahliges zwischen 1 und 2.
			for(divisor = 3; divisor <= zahl/2; divisor += 2  ) {
				// Falls Rest von Division == 0, dann pruefen, ob der neuste Divisor nicht schon ein vielfaches von alten Divisoren ist.
				if(zahl%divisor==0) {
					acceptNewDivisor = true;
					for(Long oldDivisor : allDivisors){
						if(divisor%oldDivisor==0){
							acceptNewDivisor = false;
							break;
						}
					}
					// Falls der neue Divisor wirklich neu ist (Primzahl), dann Anzahl "akzeptierte" Divisoren erhoehen.
					if(acceptNewDivisor) {
						if(debugPrint) System.out.println("New Primzahl-Divisor found: " + divisor);
						nDivisors++;
						allDivisors.add(divisor);
					}
				}
			} // Ende Loop ueber Divisoren
			
			// Rueckgabe Ergebnis
			if(debugPrint) System.out.println(new DivisorResult(zahl, nDivisors, allDivisors));
			return new DivisorResult(zahl, nDivisors, allDivisors);
		}
	}

	
	
	/**
	 * Ausfuehrung aller Divisoren-Tasks.
	 * Alle Zahlen innerhalb <von> bis <bis> berechnen (mittels for Schlaufe).
	 * Uebergabe in fixedThradPool des Executor Frameworks. 
	 * 
	 * @return
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	DivisorResult calculate() throws InterruptedException, ExecutionException {
		// Executor und Liste der Callables vorbereiten
		ExecutorService executor = Executors.newFixedThreadPool(threadCount);
		List<Callable<DivisorResult>> listOfCallables = new ArrayList<Callable<DivisorResult>>();
     
		// Loop ueber alle zahlen 'von' 'bis'. Jeweils einen neuen Callable erzeugen.
		for(long i=von; i<=bis; i++){
			// System.out.println("Creating callable for number" + i);
			listOfCallables.add(new calculateCallable(i));
		}
		// Ausfuehren aller Callables mit excecutor framework
		List<Future<DivisorResult>> listOfResults = executor.invokeAll(listOfCallables);
		if(debugPrint) System.out.println("Executor invoked all.");
		
		// Abholen der Ergebnisse.
		// Automatisches Sortieren, da resultSet ein TreeSet ist und DivisorResult das Comparable Interface implementiert.
		for(Future<DivisorResult> future : listOfResults){
			try {
				resultSet.add( future.get() );
        		}
			catch (InterruptedException | ExecutionException e) {
					e.getStackTrace();
        	}
		}
		if(debugPrint) System.out.println("Results fetched.");
		executor.shutdown();
		
		// Groesstes Ergebnis zurueckgeben. Ist einfach, da TreeSet bereits sortiert.
		return resultSet.last();
	}

	/**
	 * main methode liest benoetigte Parameter aus Arguments args[] ein und initiiert Berechnung.
	 * 
	 * @param args
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	public static void main(String[] args) throws InterruptedException,
			ExecutionException {
		if (args.length != 3) {
			System.out
					.println("Usage: CalculateDivisor <intervalStart> <intervalEnd> <threadCount>");
			System.exit(1);
		}
		boolean debugPrint = false;
		
		if(debugPrint) System.out.println("Main started...");
		long von = Long.parseLong(args[0]);
		long bis = Long.parseLong(args[1]);
		int threads = Integer.parseInt(args[2]);
		if(debugPrint) System.out.println("von="+von+ ", bis="+bis+ ", threads="+threads);
		CalculateDivisor cp = new CalculateDivisor(von, bis, threads);
		if(debugPrint) System.out.println("CalculateDivisor class created.");
		System.out.println("Ergebnis: " + cp.calculate());
	}

}

/**
 * Beinhaltet das Ergebnis einer Divisoren-Berechnugn
 * implementiert das Comparable-Interface.
 * 
 * @author Daniel Hoop & Mehrdad Zendehzaban
 *
 */
class DivisorResult implements Comparable<DivisorResult> {
	// das eigentlich ergebnis - die Zahl mit der max. Anzahl von Divisoren
	private long result;
	// Anzahl der Divisoren von Result
	private long countDiv;
	// Genaue Divisoren
	private HashSet<Long> allDiv;

	// Konstruktor
	public DivisorResult(long r, long c, HashSet<Long> a) {
		result = r;
		countDiv = c;
		allDiv = a;
	}
	// Sondierende Methoden
	public long getResult() {
		return result;
	}
	public long getCountDiv() {
		return countDiv;
	}
	public String allDivToString(){
		StringBuilder sb = new StringBuilder();
		for(Long a : allDiv)
			sb.append(a + ", ");
		String s = sb.toString();
		if(s.length()>3)
			s = s.substring(0, s.length()-2);
		return s;
	}
	
	

	@Override
	public String toString() {
		return "Zahl mit maximaler Anzahl Primzahl-Divisoren: " + result + " ("
				+ countDiv + " Primzahl-Divisoren)\n" + "Divisoren sind: " + allDivToString();
	}
	
	// Divisor Result implementiert Comparable Interface, damit sortieren in TreeSet moeglich.
	@Override
	public int compareTo(DivisorResult other){
		//System.out.println("Comparing...");
		if(this.getCountDiv() > other.getCountDiv()) {
			return 1;
		} else if (this.getCountDiv() < other.getCountDiv()){
			return -1;
		} else {
			if(this.getResult() > other.getResult()) {
				return -1;
			} else if (this.getResult() < other.getResult()){
				return 1;
			} else {
				return 0;
			}
		}
	}
}
