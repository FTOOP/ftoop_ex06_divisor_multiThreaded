package ch.ffhs.ftoop.p1.divisor;

import java.util.concurrent.ExecutionException;

//import org.junit.Rule;
//import org.junit.Test;
//import org.junit.rules.Timeout;

import student.TestCase;

/**
 * Hinweis: Die Unit Tests haben einen festen Timeout von 10 sekunden - achten
 * Sie daher darauf, dass Sie das Testintervall nicht zu gross gestalten.
 * 
 * @author ble
 * 
 */

public class CalculateDivisorTest extends TestCase {

	public void testCalculate() throws InterruptedException, ExecutionException {
		CalculateDivisor.main(new String[] { "10", "100000", "6" });
		assertFuzzyEquals(
				"Ergebnis: Zahl mit maximaler Anzahl Primzahl-Divisoren: 30030 (6 Primzahl-Divisoren)\nDivisoren sind: 2, 3, 5, 7, 11, 13\n",
				systemOut().getHistory());
	}

}
