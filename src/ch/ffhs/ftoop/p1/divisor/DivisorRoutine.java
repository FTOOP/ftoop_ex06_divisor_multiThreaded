package ch.ffhs.ftoop.p1.divisor;

import java.util.HashSet;

public class DivisorRoutine {
		long zahl;
		HashSet<Long> allDivisors;
		boolean debugPrint = true;
		
		public DivisorRoutine(long zahl){
			this.zahl = zahl;
			allDivisors = new HashSet<Long>();
		}
		
		public DivisorResult call() {
			long divisor, nDivisors = 0;
			boolean acceptNewDivisor = true;
			
			// Am Anfang pruefen, ob durch 2 teilbar. Danach kann man sich alle vielfachen von 2 sparen.
			if(zahl%2==0){
				if(debugPrint) System.out.println("New Primzahl-Divisor found: " + 2);
				nDivisors = 1;
			}
			// Der Loop muss nur bis zur H�lfte der Zahl. Danach gibt es per Definition immer etwas nicht ganzzahliges zwischen 1 und 2.
			for(divisor = 3; divisor <= zahl/2; divisor += 2  ) {
				// Falls Rest von Division == 0, dann pruefen, ob der neuste Divisor nicht schon ein vielfaches von altem Divisor ist.
				if(zahl%divisor==0) {
					acceptNewDivisor = true;
					for(Long oldDivisor : allDivisors){
						if(divisor%oldDivisor==0){
							acceptNewDivisor = false;
							break;
						}
					}
					// Falls der neue Divisor wirklich neu ist (Primzahl), dann Anzahl bestaetigte Divisoren erhoehen.
					if(acceptNewDivisor) {
						if(debugPrint) System.out.println("New Primzahl-Divisor found: " + divisor);
						nDivisors++;
						allDivisors.add(divisor);
					}
				}
			} // Ende Loop ueber Divisoren
			
			// Rueckgabe Ergebnis
			return new DivisorResult(zahl, nDivisors, allDivisors);
		}
}
